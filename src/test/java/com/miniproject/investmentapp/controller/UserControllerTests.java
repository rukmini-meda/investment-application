package com.miniproject.investmentapp.controller;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.mockito.ArgumentMatchers.any;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.miniproject.investmentapp.entity.Address;
import com.miniproject.investmentapp.entity.BankAccount;
import com.miniproject.investmentapp.entity.User;
import com.miniproject.investmentapp.mockData.MockUsers;
import com.miniproject.investmentapp.repository.UserRepository;
import com.miniproject.investmentapp.service.UserService;

import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import static org.mockito.Mockito.times;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.slf4j.Logger;

@WebMvcTest(UserController.class)
public class UserControllerTests {

    // Logger log = LoggerFactory.getLogger(UserControllerTests.class);
    
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private UserService userService;

    @Test
    public void testGetUsers() throws Exception{
        List<User> allMockUsers = new ArrayList<>();
        allMockUsers.add(MockUsers.mockUsers[0]);
        allMockUsers.add(MockUsers.mockUsers[1]);
        allMockUsers.add(MockUsers.mockUsers[2]);
        allMockUsers.add(MockUsers.mockUsers[3]);
        allMockUsers.add(MockUsers.mockUsers[4]);
        System.out.println(allMockUsers);
        Mockito.when(userService.getUsers()).thenReturn(allMockUsers);
        String url = "/users";
        MvcResult mvcResult = mockMvc.perform(get(url)).andExpect(status().isOk()).andReturn();
        String actualJsonResponse = mvcResult.getResponse().getContentAsString();
        System.out.println(actualJsonResponse);

        String expectedJsonResponse = objectMapper.writeValueAsString(allMockUsers);
        assertThat(actualJsonResponse).isEqualToIgnoringWhitespace(expectedJsonResponse);
    }

    @Test
    public void testGetUser() throws Exception{
        User mockUserWithId = new User(
            "Rukmini",
            "Meda",
            "21-12-2000",
            "female",
            "SPAP2R7890",
            new Address(
                "3-38",
                "Jaya Nagar",
                "Anantapur",
                "Andhra Pradesh",
                "India",
                "515002"
            ),
            new BankAccount(
                "DOAJS897",
                "HJBJ876877"
            )
        );
        int id = mockUserWithId.getId();
        Mockito.when(userService.getUser(id)).thenReturn(mockUserWithId);
        String url = "/users/" + id;
        MvcResult mvcResult = mockMvc.perform(get(url)).andExpect(status().isOk()).andReturn();
        String actualJsonResponse = mvcResult.getResponse().getContentAsString();
        System.out.println(actualJsonResponse);
        String expectedJsonResponse = objectMapper.writeValueAsString(mockUserWithId);
        assertThat(actualJsonResponse).isEqualToIgnoringWhitespace(expectedJsonResponse);
    }

    @Test
    public void testAddUser() throws Exception{
        User newUser = new User(
            "Rukmini",
            "Meda",
            "21-12-2000",
            "female",
            "SPAP2R7890",
            new Address(
                "3-38",
                "Jaya Nagar",
                "Anantapur",
                "Andhra Pradesh",
                "India",
                "515002"
            ),
            new BankAccount(
                "DOAJS897",
                "HJBJ876877"
            )
        );

        User savedUser = new User(
            1,
            "Rukmini",
            "Meda",
            "21-12-2000",
            "female",
            "SPAP2R7890",
            new Address(
                "3-38",
                "Jaya Nagar",
                "Anantapur",
                "Andhra Pradesh",
                "India",
                "515002"
            ),
            new BankAccount(
                "DOAJS897",
                "HJBJ876877"
            )
        );

        Mockito.when(userService.saveUser(any())).thenReturn(savedUser);

        String url = "/users";

        MvcResult mvcResult = mockMvc.perform(
            post(url)
            .contentType("application/json")
            .content(objectMapper.writeValueAsString(newUser))
            .with(csrf())
        ).andDo(print()).andExpect(status().isOk()).andReturn();

        assertThat(mvcResult.getResponse().getContentAsString()).isEqualToIgnoringWhitespace(
            objectMapper.writeValueAsString(savedUser)
        );
    }

    @Test
    public void testUserNameMustNotBeBlank() throws Exception{
        User user = new User();
        String url = "/users";
        mockMvc.perform(
            post(url)
            .contentType("application/json")
            .content(objectMapper.writeValueAsString(user))
            .with(csrf())
        ).andExpect(status().isBadRequest()).andDo(print());
        Mockito.verify(userService, times(0)).saveUser(user);
    }

    @Test
    public void testDeleteUser() throws Exception{
        int id = 1;
        String successMessage = "Successfully deleted user with id = " + id;
        Map<String,Boolean> message = new HashMap<>();
        message.put("deleted", true);
        String successMessageFromController = objectMapper.writeValueAsString(message);
        Mockito.when(userService.deleteUser(id)).thenReturn(successMessage);
        String url = "/users/" + id;
        mockMvc.perform(
            delete(url)
        ).andExpect(status().isOk()).andDo(print()).andExpect(content().string(successMessageFromController));
        Mockito.verify(userService, times(1)).deleteUser(id);
    }
}
