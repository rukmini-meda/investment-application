package com.miniproject.investmentapp.mockData;
import com.miniproject.investmentapp.entity.Address;
import com.miniproject.investmentapp.entity.BankAccount;
import com.miniproject.investmentapp.entity.User;

public class MockUsers {

    // public static User userFromJSON

    public static final User[] mockUsers = {
        new User(
            "Rukmini",
            "Meda",
            "21-12-2000",
            "female",
            "SPAP2R7890",
            new Address(
                "3-38",
                "Jaya Nagar",
                "Anantapur",
                "Andhra Pradesh",
                "India",
                "515002"
            ),
            new BankAccount(
                "DOAJS897",
                "HJBJ876877"
            )
        ),
        new User(
            "Ramya",
            "Sree",
            "23-12-2000",
            "female",
            "GJHG7897",
            new Address(
                "3-38",
                "Jaya Nagar",
                "Anantapur",
                "Andhra Pradesh",
                "India",
                "515002"
            ),
            new BankAccount(
                "ASDXQ567",
                "ASDC7879890"
            )
        ),
        new User(
            "Rukmini",
            "Meda",
            "21-12-2000",
            "female",
            "hfksfj",
            new Address(
                "3-38",
                "Jaya Nagar",
                "Andhra Pradesh",
                "Anantapur",
                "India",
                "515002"
            ),
            new BankAccount(
                "82785826",
                "jfhjasfjbaj"
            )
        ),
        new User(
        "Ramya",
        "Meda",
        "21-12-2000",
        "female",
        "hfksfj",
        new Address(
            "3-38",
            "Jaya Nagar",
            "Andhra Pradesh",
            "Anantapur",
            "India",
            "515002"
        ),
        new BankAccount(
            "82785826",
            "jfhjasfjbaj"
        )),
        new User(
         "Poojitha",
        "Meda",
        "21-12-2000",
        "female",
        "hfksfj",
        new Address(
            "3-38",
            "Jaya Nagar",
            "Andhra Pradesh",
            "Anantapur",
            "India",
            "515002"
        ),
        new BankAccount(
            "82785826",
            "jfhjasfjbaj"
        ))
    };
}
