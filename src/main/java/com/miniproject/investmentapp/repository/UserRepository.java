package com.miniproject.investmentapp.repository;

import com.miniproject.investmentapp.entity.User;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
    
}
