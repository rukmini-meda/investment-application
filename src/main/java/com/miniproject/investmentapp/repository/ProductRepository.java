package com.miniproject.investmentapp.repository;

import com.miniproject.investmentapp.entity.Product;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Integer> {
    
}
