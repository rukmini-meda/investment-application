package com.miniproject.investmentapp.entity;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;

@Entity
@AllArgsConstructor
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @NotNull
    @Size(min = 2, message = "First name should have atleast 2 characters")
    private String firstName, lastName;

    private String dateOfBirth;
    private String gender;
    
    @NotNull
    @Size(min = 3, max = 10)
    private String panNo;

    @Valid
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id", referencedColumnName = "id")
    private Address address;
    
    @Valid
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "bankaccount_id", referencedColumnName = "id")
    private BankAccount bankaccount;

    public User(){}

    

    public User(@NotNull @Size(min = 2, message = "First name should have atleast 2 characters") String firstName,
            @NotNull @Size(min = 2, message = "First name should have atleast 2 characters") String lastName,
            String dateOfBirth, String gender, @NotNull @Size(min = 3, max = 10) String panNo, @Valid Address address,
            @Valid BankAccount bankaccount) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
        this.panNo = panNo;
        this.address = address;
        this.bankaccount = bankaccount;
    }



    public User(int id,
            @NotNull @Size(min = 2, message = "First name should have atleast 2 characters") String firstName,
            @NotNull @Size(min = 2, message = "First name should have atleast 2 characters") String lastName,
            String dateOfBirth, String gender, @NotNull @Size(min = 3, max = 10) String panNo, @Valid Address address,
            @Valid BankAccount bankaccount) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
        this.panNo = panNo;
        this.address = address;
        this.bankaccount = bankaccount;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPanNo() {
        return panNo;
    }

    public void setPanNo(String panNo) {
        this.panNo = panNo;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public BankAccount getBankAccount() {
        return bankaccount;
    }

    public void setBankAccount(BankAccount bankaccount) {
        this.bankaccount = bankaccount;
    }

    @Override
    public String toString() {
        return "User [address=" + address + ", bankAccount=" + bankaccount + ", dateOfBirth=" + dateOfBirth
                + ", firstName=" + firstName + ", gender=" + gender + ", id=" + id + ", lastName=" + lastName
                + ", panNo=" + panNo + "]";
    }

    
    public int getId(){
        return id;
    }
}
