package com.miniproject.investmentapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "bankaccount")
public class BankAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @OneToOne(mappedBy = "bankaccount")
    private User user;

    @NotNull
    private String accountNo, ifscCode;

    public BankAccount(){}

    public BankAccount(String accountNo, String ifscCode) {
        this.accountNo = accountNo;
        this.ifscCode = ifscCode;
    }

    @Override
    public String toString() {
        return "BankAccount [accountNo=" + accountNo + ", ifscCode=" + ifscCode + "]";
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }
    
}
