package com.miniproject.investmentapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "address")
public class Address {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @OneToOne(mappedBy = "address")
    private User user;

    @NotNull
    private String houseNo;
    private String streetNo;
    
    @NotNull
    private String state;
    
    @NotNull
    private String place;
    
    @NotNull
    private String country;
    
    @NotNull
    @Size(min = 6, max = 6)
    private String pincode;

    

    @Override
    public String toString() {
        return "Address [country=" + country + ", houseNo=" + houseNo + ", pincode=" + pincode + ", place=" + place
                + ", state=" + state + ", streetNo=" + streetNo + "]";
    }

    public Address(){}

    public Address(String houseNo, String streetNo, String place, String state, String country, String pincode) {
        this.houseNo = houseNo;
        this.streetNo = streetNo;
        this.state = state;
        this.place = place;
        this.country = country;
        this.pincode = pincode;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public String getStreetNo() {
        return streetNo;
    }

    public void setStreetNo(String streetNo) {
        this.streetNo = streetNo;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }
}
