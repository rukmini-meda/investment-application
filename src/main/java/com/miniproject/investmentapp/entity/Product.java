package com.miniproject.investmentapp.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.miniproject.investmentapp.enums.ProductCategory;


@Entity
@Table(name = "product")
public class Product implements Serializable{

    private static final long serialVersionUID = -4439114469417994311L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @NotNull
    private String productName;
    
    @NotNull
    @Enumerated(EnumType.STRING)
    private ProductCategory category;
    
    @NotNull
    private Long minInvestmentAllowed, maxInvestmentAllowed;
    
    @NotNull
    private Boolean isActive;

    public Product() {
    }

    

    public Product(@NotNull String productName, @NotNull ProductCategory category, @NotNull Long minInvestmentAllowed,
            @NotNull Long maxInvestmentAllowed, @NotNull Boolean isActive) {
        this.productName = productName;
        this.category = category;
        this.minInvestmentAllowed = minInvestmentAllowed;
        this.maxInvestmentAllowed = maxInvestmentAllowed;
        this.isActive = isActive;
    }



    public int getId() {
        return id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public ProductCategory getCategory() {
        return category;
    }

    public void setCategory(ProductCategory category) {
        this.category = category;
    }

    public Long getMinInvestmentAllowed() {
        return minInvestmentAllowed;
    }

    public void setMinInvestmentAllowed(Long minInvestmentAllowed) {
        this.minInvestmentAllowed = minInvestmentAllowed;
    }

    public Long getMaxInvestmentAllowed() {
        return maxInvestmentAllowed;
    }

    public void setMaxInvestmentAllowed(Long maxInvestmentAllowed) {
        this.maxInvestmentAllowed = maxInvestmentAllowed;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

}
