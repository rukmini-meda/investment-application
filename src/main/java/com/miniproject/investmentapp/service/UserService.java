package com.miniproject.investmentapp.service;

import java.util.List;

import com.miniproject.investmentapp.entity.User;
import com.miniproject.investmentapp.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public User saveUser(User user) {
        System.out.println("Saving user in service");
        System.out.println(user);
        User output = userRepository.save(user);
        System.out.println(output);
        return output;
    }

    public User updateUser(User user, int id) {
        User existingUser = userRepository.findById(id).orElse(null);
        System.out.println("User to be modified is here: " + existingUser);
        if (existingUser != null) {
            if(user.getAddress() != null)
                existingUser.setAddress(user.getAddress());    
            if(user.getLastName() != null)
                existingUser.setLastName(user.getLastName());
            if(user.getFirstName() != null)
                existingUser.setFirstName(user.getFirstName());
            if(user.getBankAccount() != null)
                existingUser.setBankAccount(user.getBankAccount());
            if(user.getDateOfBirth() != null)
                existingUser.setDateOfBirth(user.getDateOfBirth());
            if(user.getGender() != null)
                existingUser.setGender(user.getGender());
            if(user.getPanNo() != null)
                existingUser.setPanNo(user.getPanNo());
            return userRepository.save(existingUser);
        } else {
            return null;
        }
    }

    public String deleteUser(int id) {
        try {
            userRepository.deleteById(id);
            return "Successfully deleted user with id = " + id;
        } catch (Exception e) {
            return "Error!";
        }
    }

    public List<User> getUsers() {
        return userRepository.findAll();
    }

    public User getUser(int id) {
        User user = userRepository.findById(id).orElse(null);
        return user;
    }

}
