package com.miniproject.investmentapp.service;

import java.util.List;

import com.miniproject.investmentapp.entity.Product;
import com.miniproject.investmentapp.exceptions.ResourceAlreadyExistsException;
import com.miniproject.investmentapp.exceptions.ResourceNotFoundException;
import com.miniproject.investmentapp.repository.ProductRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

@Service
public class ProductService {
    private ProductRepository repository;

    @Autowired
    public ProductService(ProductRepository repository) {
        this.repository = repository;
    }

    public Product saveProduct(Product product){
        return repository.save(product);
    }

    @Cacheable(value = "product", key = "#id")
    public Product getProduct(int id) {
        return repository.findById(id).orElse(null);
    }

    @Cacheable(value = "product", key = "#root.methodName")
    public List<Product> getProducts(){
        return repository.findAll();
    }

    @Cacheable(value = "product", key = "{#direction, #field}")
    public List<Product> getProductsBySorting(String direction, String field){
        if(direction.compareTo("asc") == 0){
            return repository.findAll(Sort.by(Direction.ASC, field));
        }
        else{
            return repository.findAll(Sort.by(Direction.DESC, field));
        }
    }

    @Cacheable(value = "product", key = "{#offset, #pageSize}")
    public Page<Product> getProductsWithPagination(int offset, int pageSize){
        return repository.findAll(PageRequest.of(offset, pageSize));
    }

    public Page<Product> getProductsWithPaginationAndSorting(int offset, int pageSize, String direction, String field){
        if(direction == "asc"){
            return repository.findAll(PageRequest.of(offset, pageSize, Direction.ASC, field));
        }
        else{
            return repository.findAll(PageRequest.of(offset, pageSize, Direction.DESC, field));
        }
    }

    @CachePut(value = "product", key = "#product.id")
    public Product updateProduct(Product product) throws ResourceNotFoundException{
        Product existingProduct = repository.findById(product.getId()).orElseThrow(() -> new ResourceNotFoundException("Product with this ID doesn't exists"));
        existingProduct.setCategory(product.getCategory());
        existingProduct.setIsActive(product.getIsActive());
        existingProduct.setMinInvestmentAllowed(product.getMinInvestmentAllowed());
        existingProduct.setMaxInvestmentAllowed(product.getMaxInvestmentAllowed());
        existingProduct.setProductName(product.getProductName());
        return repository.save(existingProduct);
    }
    
    @CacheEvict(value = "product", key = "#id")
    public void deleteProduct(int id) throws ResourceNotFoundException{
        if(repository.findById(id) == null){
            throw new ResourceNotFoundException("Product with this ID doesn't exists");
        }
        else{
            repository.deleteById(id);
        }
    }
}
