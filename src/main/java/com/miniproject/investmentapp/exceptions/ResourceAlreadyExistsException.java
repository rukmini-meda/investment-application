package com.miniproject.investmentapp.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class ResourceAlreadyExistsException extends Exception {
    private static final long serialVersionUID = 2L;
    public ResourceAlreadyExistsException(String message){
        super(message);
    }
}
