package com.miniproject.investmentapp.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.miniproject.investmentapp.entity.Product;
import com.miniproject.investmentapp.exceptions.ResourceNotFoundException;
import com.miniproject.investmentapp.service.ProductService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductController {
    private ProductService service;

    @Autowired
    public ProductController(ProductService service) {
        this.service = service;
    }

    @GetMapping("/products")
    public List<Product> getProducts(){
        return service.getProducts();
    }

    @GetMapping("/products/{id}")
    public Product getProduct(@PathVariable(value = "id") int id){
        return service.getProduct(id);
    }

    @PostMapping("/products")
    public Product addProduct(@RequestBody Product product){
        return service.saveProduct(product);
    }

    @PutMapping("/products")
    public ResponseEntity<Product> updateProduct(@Valid @RequestBody Product product) throws ResourceNotFoundException{
        Product updatedProduct = service.updateProduct(product);
        if(updatedProduct == null){
            throw new ResourceNotFoundException("Product not found for this id");
        }
        else{
            return ResponseEntity.ok(updatedProduct);
        }
    }

    @DeleteMapping("/products/{id}")
    public Map<String, Boolean> deleteProduct(@PathVariable int id) throws ResourceNotFoundException{
        Map<String, Boolean> response = new HashMap<>();
        try {
            service.deleteProduct(id);
            response.put("deleted", true);   
            return response; 
        } catch (Exception e) {
            response.put("deleted", false);
            return response;
        }    
    }

    @GetMapping("/products/sort/{direction}/{field}")
    public List<Product> getProductsBySorting(@PathVariable(value = "direction") String direction, @PathVariable(value = "field") String field){
        return service.getProductsBySorting(direction, field);
    }

    @GetMapping("/products/page/{offset}/{pageSize}")
    public Page<Product> getProductsWithPagination(@PathVariable(value = "offset") int offset, @PathVariable(value = "pageSize") int pageSize){
        return service.getProductsWithPagination(offset, pageSize);
    }

    @GetMapping("/products/page/sort/{offset}/{pageSize}/{direction}/{field}")
    public Page<Product> getProductsWithPaginationAndSorting(@PathVariable(value = "offset") int offset, 
        @PathVariable(value = "pageSize") int pageSize, 
        @PathVariable(value = "direction") String direction, 
        @PathVariable(value = "field") String field){
        return service.getProductsWithPaginationAndSorting(offset, pageSize, direction, field);
    }
    
}
