package com.miniproject.investmentapp.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.miniproject.investmentapp.entity.User;
import com.miniproject.investmentapp.exceptions.ResourceNotFoundException;
import com.miniproject.investmentapp.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    
    Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService service;

    @GetMapping("/users")
    public List<User> getUsers(){
        log.info("Request to fetch all users");
        return service.getUsers();
    }

    @PostMapping("/users")
    public User addUser(@Valid @RequestBody User user){
        log.info("Adding user: " + user + " through POST");
        User output = service.saveUser(user);
        log.info("Output is: " + output);
        return output;
    }

    @GetMapping("/users/{id}")
    public User getUser(@PathVariable(value = "id") int id){
        log.debug("Request path variable = {}", id);
        return service.getUser(id);
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<User> updateUser(@PathVariable int id, @RequestBody User user) throws ResourceNotFoundException{
        User updatedUser = service.updateUser(user, id);
        if(updatedUser == null){
            throw new ResourceNotFoundException("Employee not found for this id");
        }
        else{
            return ResponseEntity.ok(updatedUser);
        }
    }

    @DeleteMapping("/users/{id}")
    public Map<String, Boolean> deleteUser(@PathVariable(value = "id") int id) throws ResourceNotFoundException{
        log.debug("Request path variable = {}", id);
        String message = service.deleteUser(id);
        if(message.compareTo("Error!") == 0){
            throw new ResourceNotFoundException(message);
        }
        else{
            Map<String, Boolean> response = new HashMap<>();
            response.put("deleted", Boolean.TRUE);
            return response;
        }
    }
}
